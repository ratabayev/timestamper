package org.timestamper;

import org.timestamper.processor.TSPrinter;
import org.timestamper.processor.TSWriter;
import org.timestamper.repositories.TStampRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(Application.class);


    @Autowired
    private TStampRepository tStampRepository;


    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }


    @Override
    public void run(ApplicationArguments args) throws Exception {
        getTSProcessor(args).run();
    }

    protected final Runnable getTSProcessor(ApplicationArguments args) {
        if (args.getNonOptionArgs().contains("-p")) {
            logger.debug("TSPrinter");
            return new TSPrinter(tStampRepository);
        } else {
            logger.debug("TSWriter");
            return new TSWriter(tStampRepository);
        }
    }


}
