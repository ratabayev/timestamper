package org.timestamper.repositories;

import org.timestamper.entities.TStamp;
import org.springframework.data.repository.CrudRepository;

public interface TStampRepository extends CrudRepository<TStamp, Long> {
}

