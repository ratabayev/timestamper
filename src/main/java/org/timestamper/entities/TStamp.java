package org.timestamper.entities;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Table
@Entity
@NoArgsConstructor
public class TStamp {
    @Id
    @GeneratedValue
    private long id;
    private LocalDateTime value;

    public TStamp(LocalDateTime value) {
        this.value = value;
    }
}
