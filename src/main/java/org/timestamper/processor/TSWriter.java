package org.timestamper.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.CrudRepository;
import org.timestamper.entities.TStamp;

import java.time.LocalDateTime;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Class implements behavior of application runned without params.
 * In case of failed or slow connection to database might be thrown OutOfMemoryError.
 * It is better to use external queue to solve this issue.
 */
public class TSWriter implements Runnable {

    private final CrudRepository repository;
    private final ScheduledExecutorService consumerExecutorService;

    public TSWriter(CrudRepository repository) {
        this.repository = repository;
        this.consumerExecutorService = Executors.newSingleThreadScheduledExecutor();
    }

    @Override
    public void run() {

        // Queue in memory in order to keep all generated data properly, even if we have any problems with database
        Queue<LocalDateTime> queue = new ConcurrentLinkedQueue<>();

        // Generating new data with fixed delay
        Executors.newSingleThreadScheduledExecutor()
                .scheduleAtFixedRate(new TSWriterProducer(queue), 0, 1, TimeUnit.SECONDS);

        // First data consumption
        consumerExecutorService
                .schedule(new TSWriterConsumer(queue, repository, consumerExecutorService), 0, TimeUnit.SECONDS);
    }

    /**
     * Class implements moving data from memory to database
     */
    public static class TSWriterConsumer implements Runnable {

        private static final Logger LOGGER = LoggerFactory.getLogger(TSWriterConsumer.class);
        private static final long SLEEP_TIME = 5L;

        private final Queue<LocalDateTime> queue;
        private final CrudRepository repository;
        private final ScheduledExecutorService consumerExecutorService;

        public TSWriterConsumer(Queue<LocalDateTime> queue, CrudRepository repository, ScheduledExecutorService consumerExecutorService) {
            this.queue = queue;
            this.repository = repository;
            this.consumerExecutorService = consumerExecutorService;
        }

        @Override
        public void run() {
            long delay = 1L;
            try {
                // If there is a lot of data in the queue
                // in order to increase performance, better to move to Queue<List<LocalDateTime>>
                // and implement batch insertion within one db transaction
                while (!queue.isEmpty()) {
                    LOGGER.debug("An attempt to write timestamp {} to database.", queue.peek());
                    repository.save(new TStamp(queue.peek()));
                    queue.remove();
                }
            } catch (Exception e) { // method "save" throws only RuntimeException, so I decided to catch all exceptions
                LOGGER.error(
                        "{} - {} Error has occurred during writing data to database. Next attempt after {} seconds.",
                        e.getClass().getName(), e.getMessage(),
                        SLEEP_TIME
                );
                delay = SLEEP_TIME;
            }

            // Next data consumption
            consumerExecutorService.schedule(this, delay, TimeUnit.SECONDS);
        }
    }

    /**
     * Class implements filling the queue in memory by data
     */
    public static class TSWriterProducer implements Runnable {
        private final Queue<LocalDateTime> queue;

        public TSWriterProducer(Queue<LocalDateTime> queue) {
            this.queue = queue;
        }

        @Override
        public void run() {
            queue.add(LocalDateTime.now());
        }
    }
}
