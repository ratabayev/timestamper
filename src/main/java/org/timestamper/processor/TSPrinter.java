package org.timestamper.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.CrudRepository;

/**
 * Class implements behavior of application runned with params.
 */
public class TSPrinter implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(TSPrinter.class);

    private final CrudRepository repository;

    public TSPrinter(CrudRepository repository) {
        this.repository = repository;
    }

    @Override
    public void run() {
        repository.findAll().forEach(ts -> logger.info(ts.toString()));
    }
}
