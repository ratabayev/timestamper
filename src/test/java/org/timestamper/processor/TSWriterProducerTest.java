package org.timestamper.processor;

import org.junit.Test;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TSWriterProducerTest {

    @Test
    public void valid() {

        Queue<LocalDateTime> queue = new ConcurrentLinkedQueue<>();

        Runnable producer = new TSWriter.TSWriterProducer(queue);
        producer.run();

        assertEquals(1, queue.size());
        assertTrue(Duration.between(queue.remove(), LocalDateTime.now()).getSeconds() < 1);
    }
}
