package org.timestamper.processor;

import org.junit.Test;
import org.springframework.data.repository.CrudRepository;

import java.util.concurrent.TimeUnit;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class TWriterTest {

    @Test
    public void methodSaveCalled() throws InterruptedException {

        CrudRepository repository = mock(CrudRepository.class);

        Runnable runnable = new TSWriter(repository);

        runnable.run();

        TimeUnit.SECONDS.sleep(2L);

        verify(repository, atLeastOnce()).save(any());

        Thread.currentThread().interrupt();

    }
}
