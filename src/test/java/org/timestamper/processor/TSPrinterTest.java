package org.timestamper.processor;

import org.junit.Test;
import org.springframework.data.repository.CrudRepository;

import static org.mockito.Mockito.*;

public class TSPrinterTest {

    @Test
    public void findAlInvoked() {
        CrudRepository repository = mock(CrudRepository.class);
        Runnable printer = new TSPrinter(repository);
        printer.run();

        verify(repository, times(1)).findAll();
    }
}
