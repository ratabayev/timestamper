package org.timestamper.processor;

import org.junit.Test;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDateTime;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.Mockito.*;

public class TSWriterConsumerTest {

    @Test
    public void emptyQueue() {

        Queue<LocalDateTime> queue = new ConcurrentLinkedQueue<>();
        CrudRepository repository = mock(CrudRepository.class);
        ScheduledExecutorService consumerExecutorService = mock(ScheduledExecutorService.class);

        Runnable producer = new TSWriter.TSWriterConsumer(queue, repository, consumerExecutorService);
        producer.run();

        verify(repository, times(0)).save(any());
        verify(consumerExecutorService, times(1)).schedule(any(Runnable.class), eq(1L), eq(TimeUnit.SECONDS));
        assertTrue(queue.isEmpty());
    }

    @Test
    public void dbErrorQueue() {

        Queue<LocalDateTime> queue = new ConcurrentLinkedQueue<>();
        queue.add(LocalDateTime.now());
        CrudRepository repository = mock(CrudRepository.class);
        ScheduledExecutorService consumerExecutorService = mock(ScheduledExecutorService.class);
        when(repository.save(any())).thenThrow(new RuntimeException("DB Exception"));

        Runnable producer = new TSWriter.TSWriterConsumer(queue, repository, consumerExecutorService);
        producer.run();

        verify(repository, times(1)).save(any());
        verify(consumerExecutorService, times(1)).schedule(any(Runnable.class), eq(5L), eq(TimeUnit.SECONDS));
        assertFalse(queue.isEmpty());
    }

    @Test
    public void dbSuccessQueue() {

        Queue<LocalDateTime> queue = new ConcurrentLinkedQueue<>();
        queue.add(LocalDateTime.now());

        CrudRepository repository = mock(CrudRepository.class);
        ScheduledExecutorService consumerExecutorService = mock(ScheduledExecutorService.class);


        Runnable producer = new TSWriter.TSWriterConsumer(queue, repository, consumerExecutorService);
        producer.run();

        verify(repository, times(1)).save(any());
        verify(consumerExecutorService, times(1)).schedule(any(Runnable.class), eq(1L), eq(TimeUnit.SECONDS));
        assertTrue(queue.isEmpty());
    }
}
