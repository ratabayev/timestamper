package org.timestamper;

import org.timestamper.processor.TSPrinter;
import org.timestamper.processor.TSWriter;
import org.junit.Test;
import org.springframework.boot.ApplicationArguments;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ApplicationTests {

    @Test
    public void tsWriter() {
        Application application = new Application();
        ApplicationArguments args = mock(ApplicationArguments.class);
        Runnable runnable = application.getTSProcessor(args);
        assertEquals(runnable.getClass(), TSWriter.class);
    }

    @Test
    public void tsPrinter() {
        Application application = new Application();
        ApplicationArguments args = mock(ApplicationArguments.class);
        when(args.getNonOptionArgs()).thenReturn(Arrays.asList("-p"));
        Runnable runnable = application.getTSProcessor(args);
        assertEquals(runnable.getClass(), TSPrinter.class);
    }

}
